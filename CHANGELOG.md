# StrawberryFields Actix

- [2.0.0]
- [1.1.0] - 2024-08-10 - 11 files changed, 679 insertions(+), 391 deletions(-)
- 1.0.0 - 2024-03-10

[2.0.0]: https://gitlab.com/acefed/strawberryfields-actix/-/compare/e7b71c9d...main
[1.1.0]: https://gitlab.com/acefed/strawberryfields-actix/-/compare/91d39d03...e7b71c9d

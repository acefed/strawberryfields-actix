# StrawberryFields Actix

- [2.1.0]
- [2.0.0] - 2024-10-23 - 17 files changed, 875 insertions(+), 671 deletions(-)
- [1.1.0] - 2024-08-10 - 11 files changed, 679 insertions(+), 391 deletions(-)
- [1.0.0] - 2024-03-10

[2.1.0]: https://gitlab.com/acefed/strawberryfields-actix/-/compare/addcdaa2...main
[2.0.0]: https://gitlab.com/acefed/strawberryfields-actix/-/compare/e7b71c9d...addcdaa2
[1.1.0]: https://gitlab.com/acefed/strawberryfields-actix/-/compare/91d39d03...e7b71c9d
[1.0.0]: https://gitlab.com/acefed/strawberryfields-actix/-/blob/main/CHANGELOG.md

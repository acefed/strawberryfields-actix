#!/usr/bin/env bash

if [ -z "$ENABLE_CLONE" ] && [ -n "$GOOGLE_CLOUD_WORKSTATIONS" ]; then
  ENABLE_CLONE=true
fi
ENABLE_CLONE=$(echo "$ENABLE_CLONE" | tr '[:upper:]' '[:lower:]')
if [ "$ENABLE_CLONE" = 'true' ]; then
  git clone https://gitlab.com/acefed/strawberryfields-actix.git
fi

set -e
export LC_ALL=C

if [ "$ENABLE_CLONE" = 'true' ]; then
  cd strawberryfields-actix
fi
if [ -z "$DOCKER_HOST" ]; then
  export DOCKER_HOST=unix:///var/run/docker.sock
fi
docker run --rm \
  -v "${DOCKER_HOST#unix://}:${DOCKER_HOST#unix://}" \
  -v "$PWD:/workspace" \
  -e DOCKER_HOST \
  -w /workspace \
  buildpacksio/pack build strawberryfields-actix \
  -B paketobuildpacks/builder-jammy-base \
  --docker-host="$DOCKER_HOST" \
  --cache "type=build;name=pack-cache-strawberryfields-actix.build" \
  --cache "type=launch;name=pack-cache-strawberryfields-actix.launch" \
  -v

if [ ! -f .env.example ]; then
  curl -sLO https://gitlab.com/acefed/strawberryfields-actix/-/raw/main/.env.example
fi
if [ ! -f data/config.json.example ]; then
  curl -sLo data/config.json.example --create-dirs https://gitlab.com/acefed/strawberryfields-actix/-/raw/main/data/config.json.example
fi

if [ "$1" = 'https://example' ] || [ "$1" = 'https://www.example.com' ]; then
  echo 'This is an example.'
elif [ $# -gt 0 ]; then
  sed -e "s|https://example|$1|g" data/config.json.example > data/config.json
fi

echo "$(tr -dc a-zA-Z0-9 </dev/urandom 2>/dev/null | head -c48)" >> secret.txt
echo "SECRET=$(tail -1 secret.txt)" > .env
if command -v openssl >/dev/null; then
  if [ ! -f id_rsa ]; then
    openssl genpkey -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -out id_rsa 2>/dev/null
  fi
  if [ ! -f id_rsa.pub ]; then
    openssl rsa -pubout -in id_rsa -out id_rsa.pub 2>/dev/null
  fi
elif command -v ssh-keygen >/dev/null; then
  if [ ! -f id_rsa ]; then
    ssh-keygen -q -b 4096 -m PKCS8 -t rsa -N '' -f id_rsa
  fi
  if [ ! -f id_rsa.pub ]; then
    ssh-keygen -e -m PKCS8 -f id_rsa > id_rsa.pub
  fi
fi
echo "PRIVATE_KEY=\"$(sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' id_rsa)\"" >> .env

ENABLE_INIT=$(echo "$ENABLE_INIT" | tr '[:upper:]' '[:lower:]')
if [ "$ENABLE_INIT" = 'true' ]; then
  docker run --init -d -v "$PWD/data:/workspace/data" -p "${PORT:-8080}:${PORT:-8080}" -e PORT --env-file=.env --name=strawberryfields-actix strawberryfields-actix
else
  docker run -d -v "$PWD/data:/workspace/data" -p "${PORT:-8080}:${PORT:-8080}" -e PORT --env-file=.env --name=strawberryfields-actix strawberryfields-actix
fi
if [ "$ENABLE_CLONE" = 'true' ]; then
  cd ..
fi

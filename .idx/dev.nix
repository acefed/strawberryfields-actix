{ pkgs, ... }: {
  channel = "stable-24.05";
  packages = [
    pkgs.openssl
  ];
  env = {};
  idx = {
    extensions = [
      "ms-azuretools.vscode-docker"
      "saoudrizwan.claude-dev"
    ];
    previews = {
      enable = true;
      previews = {
        web = {
          command = [
            "bash"
            "install.sh"
            "https://$PORT-$WEB_HOST"
          ];
          manager = "web";
        };
      };
    };
  };
  services.docker.enable = true;
}

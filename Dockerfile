FROM clux/muslrust:stable AS builder

WORKDIR /volume
COPY . .

ENV RUSTFLAGS="-C strip=symbols"
RUN cargo build --release

FROM gcr.io/distroless/static

WORKDIR /app
COPY --from=builder /volume/data ./data
COPY --from=builder /volume/static ./static
COPY --from=builder /volume/target/x86_64-unknown-linux-musl/release/strawberryfields-actix .

ENTRYPOINT ["/app/strawberryfields-actix"]

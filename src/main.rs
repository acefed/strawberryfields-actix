use actix_files::Files;
use actix_web::http::{StatusCode, Uri};
use actix_web::web::{self, Data, JsonConfig, Redirect};
use actix_web::{get, post, routes, App, HttpRequest, HttpResponse, HttpServer, Responder};
use base64::{prelude::BASE64_STANDARD, Engine};
use chrono::{SecondsFormat, Utc};
use dotenvy::dotenv;
use reqwest::header::{HeaderMap, HeaderName, HeaderValue};
use reqwest::Client;
use rsa::pkcs1v15::SigningKey;
use rsa::pkcs8::{DecodePrivateKey, EncodePublicKey, LineEnding};
use rsa::sha2::{Digest, Sha256};
use rsa::signature::{SignatureEncoding, Signer};
use rsa::RsaPrivateKey;
use serde_json::{json, Value};
use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};
use std::{env, fs, io};

fn uuidv7() -> String {
    let mut v = [0u8; 16];
    getrandom::getrandom(&mut v).unwrap();
    let ts = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() as u64;
    v[0] = (ts >> 40) as u8;
    v[1] = (ts >> 32) as u8;
    v[2] = (ts >> 24) as u8;
    v[3] = (ts >> 16) as u8;
    v[4] = (ts >> 8) as u8;
    v[5] = ts as u8;
    v[6] = v[6] & 0x0F | 0x70;
    v[8] = v[8] & 0x3F | 0x80;
    v.iter().map(|b| format!("{b:02x}")).collect::<String>()
}

fn talk_script(req: String) -> String {
    let u = req.to_lowercase().parse::<Uri>().unwrap_or_default();
    let ts = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() as u64;
    let res = if u.host().unwrap_or("") != "localhost" {
        [
            "<p>",
            r#"<a href="https://"#,
            u.host().unwrap_or(""),
            r#"/" rel="nofollow noopener noreferrer" target="_blank">"#,
            u.host().unwrap_or(""),
            "</a>",
            "</p>",
        ]
        .join("")
    } else {
        format!("<p>{ts}</p>")
    };
    res
}

async fn get_activity(username: String, hostname: String, req: String) -> String {
    let mut private_key_pem = env::var("PRIVATE_KEY").unwrap();
    private_key_pem = private_key_pem.split("\\n").collect::<Vec<&str>>().join("\n");
    private_key_pem = private_key_pem.trim_start_matches('"').into();
    private_key_pem = private_key_pem.trim_end_matches('"').into();
    let private_key = RsaPrivateKey::from_pkcs8_pem(&private_key_pem).unwrap();

    let u = req.parse::<Uri>().unwrap_or_default();
    let t = Utc::now().format("%a, %d %b %Y %T GMT").to_string();
    let sig = SigningKey::<Sha256>::new(private_key);
    let b64 = BASE64_STANDARD.encode(
        sig.sign(
            &[
                format!("(request-target): get {}", u.path()),
                format!("host: {}", u.host().unwrap_or("")),
                format!("date: {t}"),
            ]
            .join("\n")
            .into_bytes(),
        )
        .to_bytes(),
    );
    let headers = [
        ("Date", t),
        (
            "Signature",
            [
                format!(r#"keyId="https://{hostname}/u/{username}#Key""#),
                r#"algorithm="rsa-sha256""#.to_string(),
                r#"headers="(request-target) host date""#.to_string(),
                format!(r#"signature="{b64}""#),
            ]
            .join(","),
        ),
        ("Accept", "application/activity+json".to_string()),
        ("Accept-Encoding", "identity".to_string()),
        ("Cache-Control", "no-cache".to_string()),
        ("User-Agent", format!("StrawberryFields-Actix/2.1.0 (https://{hostname}/)")),
    ];
    let mut header_map = HeaderMap::new();
    for (k, v) in headers.iter() {
        header_map
            .insert(HeaderName::from_bytes(k.as_bytes()).unwrap(), HeaderValue::from_bytes(v.as_bytes()).unwrap());
    }
    let client = Client::new();
    let res = client.get(&req).headers(header_map).send().await.unwrap();
    let status = res.status().as_u16();
    println!("GET {req} {status}");
    res.text().await.unwrap()
}

async fn post_activity(username: String, hostname: String, req: String, x: Value) {
    let mut private_key_pem = env::var("PRIVATE_KEY").unwrap();
    private_key_pem = private_key_pem.split("\\n").collect::<Vec<&str>>().join("\n");
    private_key_pem = private_key_pem.trim_start_matches('"').into();
    private_key_pem = private_key_pem.trim_end_matches('"').into();
    let private_key = RsaPrivateKey::from_pkcs8_pem(&private_key_pem).unwrap();

    let u = req.parse::<Uri>().unwrap_or_default();
    let t = Utc::now().format("%a, %d %b %Y %T GMT").to_string();
    let body = serde_json::to_string(&x).unwrap();
    let mut digest = Sha256::new();
    digest.update(serde_json::to_vec(&x).unwrap());
    let s256 = BASE64_STANDARD.encode(digest.finalize());
    let sig = SigningKey::<Sha256>::new(private_key);
    let b64 = BASE64_STANDARD.encode(
        sig.sign(
            &[
                format!("(request-target): post {}", u.path()),
                format!("host: {}", u.host().unwrap_or("")),
                format!("date: {t}"),
                format!("digest: SHA-256={s256}"),
            ]
            .join("\n")
            .into_bytes(),
        )
        .to_bytes(),
    );
    let headers = vec![
        ("Date", t),
        ("Digest", format!("SHA-256={s256}")),
        (
            "Signature",
            [
                format!(r#"keyId="https://{hostname}/u/{username}#Key""#),
                r#"algorithm="rsa-sha256""#.to_string(),
                r#"headers="(request-target) host date digest""#.to_string(),
                format!(r#"signature="{b64}""#),
            ]
            .join(","),
        ),
        ("Accept", "application/json".to_string()),
        ("Accept-Encoding", "gzip".to_string()),
        ("Cache-Control", "max-age=0".to_string()),
        ("Content-Type", "application/activity+json".to_string()),
        ("User-Agent", format!("StrawberryFields-Actix/2.1.0 (https://{hostname}/)")),
    ];
    println!("POST {req} {body}");
    let mut header_map = HeaderMap::new();
    for (k, v) in headers.iter() {
        header_map
            .insert(HeaderName::from_bytes(k.as_bytes()).unwrap(), HeaderValue::from_bytes(v.as_bytes()).unwrap());
    }
    let client = Client::new();
    client.post(req).headers(header_map).body(body).send().await.unwrap();
}

async fn accept_follow(username: String, hostname: String, x: Value, y: Value) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}"),
        "type": "Accept",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": y
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn follow(username: String, hostname: String, x: Value) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}"),
        "type": "Follow",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": x["id"].as_str().unwrap()
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn undo_follow(username: String, hostname: String, x: Value) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}#Undo"),
        "type": "Undo",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Follow",
            "actor": format!("https://{hostname}/u/{username}"),
            "object": x["id"].as_str().unwrap()
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn like(username: String, hostname: String, x: Value, y: Value) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}"),
        "type": "Like",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": x["id"].as_str().unwrap()
    });
    post_activity(username, hostname, y["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn undo_like(username: String, hostname: String, x: Value, y: Value) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}#Undo"),
        "type": "Undo",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Like",
            "actor": format!("https://{hostname}/u/{username}"),
            "object": x["id"].as_str().unwrap()
        }
    });
    post_activity(username, hostname, y["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn announce(username: String, hostname: String, x: Value, y: Value) {
    let aid = uuidv7();
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}/activity"),
        "type": "Announce",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": x["id"].as_str().unwrap()
    });
    post_activity(username, hostname, y["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn undo_announce(username: String, hostname: String, x: Value, y: Value, z: String) {
    let aid = uuidv7();
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}#Undo"),
        "type": "Undo",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": {
            "id": format!("{z}/activity"),
            "type": "Announce",
            "actor": format!("https://{hostname}/u/{username}"),
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")],
            "object": x["id"].as_str().unwrap()
        }
    });
    post_activity(username, hostname, y["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn create_note(username: String, hostname: String, x: Value, y: String) {
    let aid = uuidv7();
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}/activity"),
        "type": "Create",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Note",
            "attributedTo": format!("https://{hostname}/u/{username}"),
            "content": talk_script(y),
            "url": format!("https://{hostname}/u/{username}/s/{aid}"),
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")]
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn create_note_image(username: String, hostname: String, x: Value, y: String, z: String) {
    let aid = uuidv7();
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}/activity"),
        "type": "Create",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Note",
            "attributedTo": format!("https://{hostname}/u/{username}"),
            "content": talk_script("https://localhost".to_string()),
            "url": format!("https://{hostname}/u/{username}/s/{aid}"),
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")],
            "attachment": [
                {
                    "type": "Image",
                    "mediaType": z,
                    "url": y
                }
            ]
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn create_note_mention(username: String, hostname: String, x: Value, y: Value, z: String) {
    let aid = uuidv7();
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let u = y["inbox"].as_str().unwrap().to_string().parse::<Uri>().unwrap_or_default();
    let at = format!("@{}@{}", y["preferredUsername"].as_str().unwrap(), u.host().unwrap_or(""));
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("https://{hostname}/u/{username}/s/{aid}/activity"),
        "type": "Create",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Note",
            "attributedTo": format!("https://{hostname}/u/{username}"),
            "inReplyTo": x["id"].as_str().unwrap(),
            "content": talk_script(z),
            "url": format!("https://{hostname}/u/{username}/s/{aid}"),
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")],
            "tag": [
                {
                    "type": "Mention",
                    "name": at
                }
            ]
        }
    });
    post_activity(username, hostname, y["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn create_note_hashtag(username: String, hostname: String, x: Value, y: String, z: String) {
    let aid = uuidv7();
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let body = json!({
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            { "Hashtag": "as:Hashtag" }
        ],
        "id": format!("https://{hostname}/u/{username}/s/{aid}/activity"),
        "type": "Create",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": {
            "id": format!("https://{hostname}/u/{username}/s/{aid}"),
            "type": "Note",
            "attributedTo": format!("https://{hostname}/u/{username}"),
            "content": talk_script(y),
            "url": format!("https://{hostname}/u/{username}/s/{aid}"),
            "published": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")],
            "tag": [
                {
                    "type": "Hashtag",
                    "name": format!("#{z}")
                }
            ]
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn update_note(username: String, hostname: String, x: Value, y: String) {
    let t = Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true);
    let ts = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() as u64;
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("{y}#{ts}"),
        "type": "Update",
        "actor": format!("https://{hostname}/u/{username}"),
        "published": t,
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "cc": [format!("https://{hostname}/u/{username}/followers")],
        "object": {
            "id": y,
            "type": "Note",
            "attributedTo": format!("https://{hostname}/u/{username}"),
            "content": talk_script("https://localhost".to_string()),
            "url": y,
            "updated": t,
            "to": ["https://www.w3.org/ns/activitystreams#Public"],
            "cc": [format!("https://{hostname}/u/{username}/followers")]
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

async fn delete_tombstone(username: String, hostname: String, x: Value, y: String) {
    let body = json!({
        "@context": "https://www.w3.org/ns/activitystreams",
        "id": format!("{y}#Delete"),
        "type": "Delete",
        "actor": format!("https://{hostname}/u/{username}"),
        "object": {
            "id": y,
            "type": "Tombstone"
        }
    });
    post_activity(username, hostname, x["inbox"].as_str().unwrap().to_string(), body).await;
}

#[get("/u/{name}")]
async fn u_user(config: web::Data<Value>, path: web::Path<String>, req: HttpRequest) -> impl Responder {
    let mine = config["actor"][0]["me"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let me = [
        r#"<a href="https://"#,
        mine.host().unwrap_or(""),
        r#"/" rel="me nofollow noopener noreferrer" target="_blank">"#,
        "https://",
        mine.host().unwrap_or(""),
        "/",
        "</a>",
    ]
    .join("");
    let mut private_key_pem = env::var("PRIVATE_KEY").unwrap();
    private_key_pem = private_key_pem.split("\\n").collect::<Vec<&str>>().join("\n");
    private_key_pem = private_key_pem.trim_start_matches('"').into();
    private_key_pem = private_key_pem.trim_end_matches('"').into();
    let private_key = RsaPrivateKey::from_pkcs8_pem(&private_key_pem).unwrap();
    let public_key_pem = private_key.to_public_key().to_public_key_pem(LineEnding::default()).unwrap();

    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = path.into_inner().to_lowercase();
    let hostname = u.host().unwrap_or("");
    let accept_header_value = req.headers().get("accept").unwrap().to_str().unwrap();
    let mut has_type = false;
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    if accept_header_value.contains("application/activity+json") {
        has_type = true;
    }
    if accept_header_value.contains("application/ld+json") {
        has_type = true;
    }
    if accept_header_value.contains("application/json") {
        has_type = true;
    }
    if !has_type {
        let body = format!("{}: {}", username, config["actor"][0]["name"].as_str().unwrap());
        return HttpResponse::Ok()
            .insert_header((
                "Cache-Control",
                format!("public, max-age={}, must-revalidate", config["ttl"].as_u64().unwrap()),
            ))
            .insert_header(("Vary", "Accept, Accept-Encoding"))
            .body(body);
    }
    let body = json!({
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            {
                "schema": "https://schema.org/",
                "PropertyValue": "schema:PropertyValue",
                "value": "schema:value",
                "Key": "sec:Key"
            }
        ],
        "id": format!("https://{hostname}/u/{username}"),
        "type": "Person",
        "inbox": format!("https://{hostname}/u/{username}/inbox"),
        "outbox": format!("https://{hostname}/u/{username}/outbox"),
        "following": format!("https://{hostname}/u/{username}/following"),
        "followers": format!("https://{hostname}/u/{username}/followers"),
        "preferredUsername": username,
        "name": config["actor"][0]["name"].as_str().unwrap(),
        "summary": "<p>2.1.0</p>",
        "url": format!("https://{hostname}/u/{username}"),
        "endpoints": {
            "sharedInbox": format!("https://{hostname}/u/{username}/inbox")
        },
        "attachment": [
            {
                "type": "PropertyValue",
                "name": "me",
                "value": me
            }
        ],
        "icon": {
            "type": "Image",
            "mediaType": "image/png",
            "url": format!("https://{hostname}/static/{username}u.png")
        },
        "image": {
            "type": "Image",
            "mediaType": "image/png",
            "url": format!("https://{hostname}/static/{username}s.png")
        },
        "publicKey": {
            "id": format!("https://{hostname}/u/{username}#Key"),
            "type": "Key",
            "owner": format!("https://{hostname}/u/{username}"),
            "publicKeyPem": public_key_pem
        }
    });
    HttpResponse::Ok()
        .content_type("application/activity+json")
        .insert_header((
            "Cache-Control",
            format!("public, max-age={}, must-revalidate", config["ttl"].as_u64().unwrap()),
        ))
        .insert_header(("Vary", "Accept, Accept-Encoding"))
        .json(body)
}

#[get("/u/{name}/inbox")]
async fn get_inbox() -> impl Responder {
    HttpResponse::MethodNotAllowed().finish()
}

#[post("/u/{name}/inbox")]
async fn post_inbox(
    config: web::Data<Value>,
    y: web::Json<Value>,
    path: web::Path<String>,
    req: HttpRequest,
) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = path.into_inner().to_lowercase();
    let hostname = u.host().unwrap_or("");
    let content_type_header_value = req.headers().get("content-type").unwrap().to_str().unwrap();
    let mut has_type = false;
    let ny: Value = y.into_inner();
    let mut t = ny["type"].as_str().unwrap_or("");
    let aid = ny["id"].as_str().unwrap_or("");
    let atype = ny["type"].as_str().unwrap_or("");
    if aid.len() > 1024 || atype.len() > 64 {
        return HttpResponse::BadRequest().finish();
    }
    println!("INBOX {aid} {atype}");
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    if content_type_header_value.contains("application/activity+json") {
        has_type = true;
    }
    if content_type_header_value.contains("application/ld+json") {
        has_type = true;
    }
    if content_type_header_value.contains("application/json") {
        has_type = true;
    }
    if !has_type {
        return HttpResponse::BadRequest().finish();
    }
    if req.headers().get("digest").map_or(true, |v| v.is_empty()) {
        return HttpResponse::BadRequest().finish();
    }
    if req.headers().get("signature").map_or(true, |v| v.is_empty()) {
        return HttpResponse::BadRequest().finish();
    }
    if t == "Accept" || t == "Reject" || t == "Add" {
        return HttpResponse::Ok().finish();
    }
    if t == "Remove" || t == "Like" || t == "Announce" {
        return HttpResponse::Ok().finish();
    }
    if t == "Create" || t == "Update" || t == "Delete" {
        return HttpResponse::Ok().finish();
    }
    if t == "Follow" {
        let ux = ny["actor"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if ux.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let x = get_activity(username.clone(), hostname.to_string(), ux.to_string()).await;
        if x.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let nx = serde_json::from_str(&x).unwrap();
        accept_follow(username, hostname.to_string(), nx, ny).await;
        return HttpResponse::Ok().finish();
    }
    if t == "Undo" {
        let nz = ny["object"].clone();
        t = nz["type"].as_str().unwrap_or("");
        if t == "Accept" || t == "Like" || t == "Announce" {
            return HttpResponse::Ok().finish();
        }
        if t == "Follow" {
            let ux = ny["actor"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
            if ux.scheme_str().unwrap_or("") != "https" {
                return HttpResponse::BadRequest().finish();
            }
            let x = get_activity(username.clone(), hostname.to_string(), ux.to_string()).await;
            if x.is_empty() {
                return HttpResponse::InternalServerError().finish();
            }
            let nx = serde_json::from_str(&x).unwrap();
            accept_follow(username, hostname.to_string(), nx, nz).await;
            return HttpResponse::Ok().finish();
        }
    }
    HttpResponse::InternalServerError().finish()
}

#[get("/u/{name}/outbox")]
async fn outbox(
    config: web::Data<Value>,
    path: web::Path<String>,
    query_params: web::Query<HashMap<String, String>>,
) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = path.into_inner().to_lowercase();
    let hostname = u.host().unwrap_or("");
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    let page = match query_params.get("page") {
        Some(_) => query_params["page"].to_string().to_lowercase(),
        None => "".to_string(),
    };
    let body = if page == "true" {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/outbox?page=true"),
            "type": "OrderedCollectionPage",
            "partOf": format!("https://{hostname}/u/{username}/outbox"),
            "orderedItems": []
        })
    } else {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/outbox"),
            "type": "OrderedCollection",
            "totalItems": 0,
            "first": format!("https://{hostname}/u/{username}/outbox?page=true"),
            "last": format!("https://{hostname}/u/{username}/outbox?page=true")
        })
    };
    HttpResponse::Ok().content_type("application/activity+json").json(body)
}

#[get("/u/{name}/following")]
async fn following(
    config: web::Data<Value>,
    path: web::Path<String>,
    query_params: web::Query<HashMap<String, String>>,
) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = path.into_inner().to_lowercase();
    let hostname = u.host().unwrap_or("");
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    let page = match query_params.get("page") {
        Some(_) => query_params["page"].to_string().to_lowercase(),
        None => "".to_string(),
    };
    let body = if page == "true" {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/following?page=true"),
            "type": "OrderedCollectionPage",
            "partOf": format!("https://{hostname}/u/{username}/following"),
            "orderedItems": []
        })
    } else {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/following"),
            "type": "OrderedCollection",
            "totalItems": 0,
            "first": format!("https://{hostname}/u/{username}/following?page=true"),
            "last": format!("https://{hostname}/u/{username}/following?page=true")
        })
    };
    HttpResponse::Ok().content_type("application/activity+json").json(body)
}

#[get("/u/{name}/followers")]
async fn followers(
    config: web::Data<Value>,
    path: web::Path<String>,
    query_params: web::Query<HashMap<String, String>>,
) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = path.into_inner().to_lowercase();
    let hostname = u.host().unwrap_or("");
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    let page = match query_params.get("page") {
        Some(_) => query_params["page"].to_string().to_lowercase(),
        None => "".to_string(),
    };
    let body = if page == "true" {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/followers?page=true"),
            "type": "OrderedCollectionPage",
            "partOf": format!("https://{hostname}/u/{username}/followers"),
            "orderedItems": []
        })
    } else {
        json!({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": format!("https://{hostname}/u/{username}/followers"),
            "type": "OrderedCollection",
            "totalItems": 0,
            "first": format!("https://{hostname}/u/{username}/followers?page=true"),
            "last": format!("https://{hostname}/u/{username}/followers?page=true")
        })
    };
    HttpResponse::Ok().content_type("application/activity+json").json(body)
}

#[post("/s/{secret}/u/{name}")]
async fn s_send(
    config: web::Data<Value>,
    body: web::Json<Value>,
    path: web::Path<(String, String)>,
) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let (secret, username) = path.into_inner();
    let username = username.to_lowercase();
    let hostname = u.host().unwrap_or("");
    let send = body.into_inner();
    let t = send["type"].as_str().unwrap_or("");
    if username != config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase() {
        return HttpResponse::NotFound().finish();
    }
    if secret.is_empty() || secret == "-" {
        return HttpResponse::NotFound().finish();
    }
    if secret != env::var("SECRET").unwrap() {
        return HttpResponse::NotFound().finish();
    }
    let ux = send["id"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
    if ux.scheme_str().unwrap_or("") != "https" {
        return HttpResponse::BadRequest().finish();
    }
    let x = get_activity(username.clone(), hostname.to_string(), ux.to_string()).await;
    if x.is_empty() {
        return HttpResponse::InternalServerError().finish();
    }
    let nx: Value = serde_json::from_str(&x).unwrap();
    let aid = nx["id"].as_str().unwrap_or("");
    let atype = nx["type"].as_str().unwrap_or("");
    if aid.len() > 1024 || atype.len() > 64 {
        return HttpResponse::BadRequest().finish();
    }
    if t == "follow" {
        follow(username, hostname.to_string(), nx).await;
        return HttpResponse::Ok().finish();
    }
    if t == "undo_follow" {
        undo_follow(username, hostname.to_string(), nx).await;
        return HttpResponse::Ok().finish();
    }
    if t == "like" {
        let uy = nx["attributedTo"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let y = get_activity(username.clone(), hostname.to_string(), uy.to_string()).await;
        if y.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let ny = serde_json::from_str(&y).unwrap();
        like(username, hostname.to_string(), nx, ny).await;
        return HttpResponse::Ok().finish();
    }
    if t == "undo_like" {
        let uy = nx["attributedTo"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let y = get_activity(username.clone(), hostname.to_string(), uy.to_string()).await;
        if y.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let ny = serde_json::from_str(&y).unwrap();
        undo_like(username, hostname.to_string(), nx, ny).await;
        return HttpResponse::Ok().finish();
    }
    if t == "announce" {
        let uy = nx["attributedTo"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let y = get_activity(username.clone(), hostname.to_string(), uy.to_string()).await;
        if y.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let ny = serde_json::from_str(&y).unwrap();
        announce(username, hostname.to_string(), nx, ny).await;
        return HttpResponse::Ok().finish();
    }
    if t == "undo_announce" {
        let uy = nx["attributedTo"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let y = get_activity(username.clone(), hostname.to_string(), uy.to_string()).await;
        if y.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let ny = serde_json::from_str(&y).unwrap();
        let uz = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or(&format!("https://{hostname}/u/{username}/s/00000000000000000000000000000000"))
            .parse::<Uri>()
            .unwrap_or_default();
        if uz.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        undo_announce(username, hostname.to_string(), nx, ny, uz.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "create_note" {
        let uy = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or("https://localhost")
            .parse::<Uri>()
            .unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        create_note(username, hostname.to_string(), nx, uy.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "create_note_image" {
        let uy = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or(&format!("https://{}/static/logo.png", hostname))
            .parse::<Uri>()
            .unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" || uy.host().unwrap_or("") != hostname {
            return HttpResponse::BadRequest().finish();
        }
        let z = if uy.to_string().ends_with(".jpg") || uy.to_string().ends_with(".jpeg") {
            "image/jpeg"
        } else if uy.to_string().ends_with(".svg") {
            "image/svg+xml"
        } else if uy.to_string().ends_with(".gif") {
            "image/gif"
        } else if uy.to_string().ends_with(".webp") {
            "image/webp"
        } else if uy.to_string().ends_with(".avif") {
            "image/avif"
        } else {
            "image/png"
        };
        create_note_image(username, hostname.to_string(), nx, uy.to_string(), z.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "create_note_mention" {
        let uy = nx["attributedTo"].as_str().unwrap_or("").parse::<Uri>().unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let y = get_activity(username.clone(), hostname.to_string(), uy.to_string()).await;
        if y.is_empty() {
            return HttpResponse::InternalServerError().finish();
        }
        let ny = serde_json::from_str(&y).unwrap();
        let uz = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or("https://localhost")
            .parse::<Uri>()
            .unwrap_or_default();
        if uz.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        create_note_mention(username, hostname.to_string(), nx, ny, uz.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "create_note_hashtag" {
        let uy = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or("https://localhost")
            .parse::<Uri>()
            .unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        let z = send.get("tag").and_then(Value::as_str).filter(|v| !v.is_empty()).unwrap_or("Hashtag");
        create_note_hashtag(username, hostname.to_string(), nx, uy.to_string(), z.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "update_note" {
        let uy = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or(&format!("https://{hostname}/u/{username}/s/00000000000000000000000000000000"))
            .parse::<Uri>()
            .unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        update_note(username, hostname.to_string(), nx, uy.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    if t == "delete_tombstone" {
        let uy = send
            .get("url")
            .and_then(Value::as_str)
            .filter(|v| !v.is_empty())
            .unwrap_or(&format!("https://{hostname}/u/{username}/s/00000000000000000000000000000000"))
            .parse::<Uri>()
            .unwrap_or_default();
        if uy.scheme_str().unwrap_or("") != "https" {
            return HttpResponse::BadRequest().finish();
        }
        delete_tombstone(username, hostname.to_string(), nx, uy.to_string()).await;
        return HttpResponse::Ok().finish();
    }
    println!("TYPE {aid} {atype}");
    HttpResponse::Ok().finish()
}

#[get("/.well-known/nodeinfo")]
async fn nodeinfo(config: web::Data<Value>) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let hostname = u.host().unwrap_or("");
    let body = json!({
        "links": [
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                "href": format!("https://{hostname}/nodeinfo/2.0.json")
            },
            {
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1",
                "href": format!("https://{hostname}/nodeinfo/2.1.json")
            }
        ]
    });
    HttpResponse::Ok()
        .insert_header((
            "Cache-Control",
            format!("public, max-age={}, must-revalidate", config["ttl"].as_u64().unwrap()),
        ))
        .insert_header(("Vary", "Accept, Accept-Encoding"))
        .json(body)
}

#[get("/.well-known/webfinger")]
async fn webfinger(config: web::Data<Value>, query_params: web::Query<HashMap<String, String>>) -> impl Responder {
    let u = config["origin"].as_str().unwrap_or("").to_lowercase().parse::<Uri>().unwrap_or_default();
    let username = config["actor"][0]["preferredUsername"].as_str().unwrap().to_lowercase();
    let hostname = u.host().unwrap_or("");
    let p443 = format!("https://{hostname}:443/");
    let mut resource_value = match query_params.get("resource") {
        Some(_) => query_params["resource"].to_string().to_lowercase(),
        None => "".to_string(),
    };
    let mut has_resource = false;
    if resource_value.starts_with(&p443) {
        resource_value = format!("https://{}/{}", hostname, resource_value.trim_start_matches(&p443));
    }
    if resource_value == format!("acct:{username}@{hostname}") {
        has_resource = true;
    }
    if resource_value == format!("mailto:{username}@{hostname}") {
        has_resource = true;
    }
    if resource_value == format!("https://{hostname}/@{username}") {
        has_resource = true;
    }
    if resource_value == format!("https://{hostname}/u/{username}") {
        has_resource = true;
    }
    if resource_value == format!("https://{hostname}/user/{username}") {
        has_resource = true;
    }
    if resource_value == format!("https://{hostname}/users/{username}") {
        has_resource = true;
    }
    if !has_resource {
        return HttpResponse::NotFound().finish();
    }
    let body = json!({
        "subject": format!("acct:{username}@{hostname}"),
        "aliases": [
            format!("mailto:{username}@{hostname}"),
            format!("https://{hostname}/@{username}"),
            format!("https://{hostname}/u/{username}"),
            format!("https://{hostname}/user/{username}"),
            format!("https://{hostname}/users/{username}")
        ],
        "links": [
            {
                "rel": "self",
                "type": "application/activity+json",
                "href": format!("https://{hostname}/u/{username}")
            },
            {
                "rel": "http://webfinger.net/rel/avatar",
                "type": "image/png",
                "href": format!("https://{hostname}/static/{username}u.png")
            },
            {
                "rel": "http://webfinger.net/rel/profile-page",
                "type": "text/plain",
                "href": format!("https://{hostname}/u/{username}")
            }
        ]
    });
    HttpResponse::Ok()
        .content_type("application/jrd+json")
        .insert_header((
            "Cache-Control",
            format!("public, max-age={}, must-revalidate", config["ttl"].as_u64().unwrap()),
        ))
        .insert_header(("Vary", "Accept, Accept-Encoding"))
        .json(body)
}

#[routes]
#[get("/users/{name}")]
#[get("/user/{name}")]
#[get("/@{name}")]
async fn uu(path: web::Path<String>) -> impl Responder {
    let name = path.into_inner();
    Redirect::to(format!("/u/{name}")).using_status_code(StatusCode::FOUND)
}

#[actix_web::main]
async fn main() -> io::Result<()> {
    dotenv().ok();
    let config_json = if env::var("CONFIG_JSON").is_ok() {
        let mut result = env::var("CONFIG_JSON").unwrap();
        result = result.trim_start_matches("'").into();
        result = result.trim_end_matches("'").into();
        result
    } else {
        fs::read_to_string("data/config.json")?
    };
    let config = serde_json::from_str::<Value>(&config_json)?;

    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(config.clone()))
            .app_data(Data::new(JsonConfig::default().content_type_required(false)))
            .service(web::resource("/").route(web::get().to(|| async { "StrawberryFields Actix" })))
            .service(web::resource("/about").route(web::get().to(|| async { "About: Blank" })))
            .service(u_user)
            .service(get_inbox)
            .service(post_inbox)
            .service(outbox)
            .service(following)
            .service(followers)
            .service(s_send)
            .service(nodeinfo)
            .service(webfinger)
            .service(web::redirect("/@", "/").using_status_code(StatusCode::FOUND))
            .service(web::redirect("/u", "/").using_status_code(StatusCode::FOUND))
            .service(web::redirect("/user", "/").using_status_code(StatusCode::FOUND))
            .service(web::redirect("/users", "/").using_status_code(StatusCode::FOUND))
            .service(uu)
            .service(Files::new("/", "static"))
    })
    .bind(("0.0.0.0", env::var("PORT").unwrap_or("8080".to_string()).parse::<u16>().unwrap()))?
    .run()
    .await
}

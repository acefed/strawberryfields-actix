# Federation

## Supported federation protocols and standards

- [ActivityPub](https://www.w3.org/TR/activitypub/) - W3C: Recommendation - Server-to-Server
- [WebFinger](https://webfinger.net/) - IETF: Proposed Standard - [RFC 7033](https://www.rfc-editor.org/rfc/rfc7033.html)
- [HTTP Signatures](https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures) - Draft: Standards Track - Obsoleted by [RFC 9421](https://www.rfc-editor.org/rfc/rfc9421.html)
- [NodeInfo](https://nodeinfo.diaspora.software/) - 2.0, 2.1(Latest)

## Supported FEPs

- [FEP-f1d5: NodeInfo in Fediverse Software](https://codeberg.org/fediverse/fep/src/branch/main/fep/f1d5/fep-f1d5.md) - FINAL
- [FEP-67ff: FEDERATION.md](https://codeberg.org/fediverse/fep/src/branch/main/fep/67ff/fep-67ff.md) - FINAL

# StrawberryFields Actix

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/acefed/strawberryfields-actix)

[![Open in IDX](https://cdn.idx.dev/btn/open_light_32.svg)](https://idx.google.com/import?url=https%3A%2F%2Fgitlab.com%2Facefed%2Fstrawberryfields-actix)

[https://acefed.gitlab.io/strawberryfields](https://acefed.gitlab.io/strawberryfields)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/acefed/strawberryfields-actix/-/raw/main/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
StrawberryFields Actix
```

[https://acefed.gitlab.io/strawberryfields/strawberryfields-actix.spdx](https://acefed.gitlab.io/strawberryfields/strawberryfields-actix.spdx)  
[https://acefed.gitlab.io/strawberryfields/strawberryfields-actix.json](https://acefed.gitlab.io/strawberryfields/strawberryfields-actix.json)

SPDX-License-Identifier: MIT
